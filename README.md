GoogleCalendarSync - Author Siddharth Gupta
==========================================


Steps to run:

0. First step would be to create new Google API ClientId. To do this, perform following steps:
    0. In a Web Browser visit : 'https://console.developers.google.com/' and login with your credentials.
    0. Now, first you will need to create a new project. To do this click 'Create Project'. Give a project a name. Let's say : GoogleCalendarSync.
    0. Go to Project page. On the left panel, click on APIs & Auth.
    0. Under 'APIs & Auth', click on 'API' link.
    0. Search for 'Calendar API' and click on the link. You would then see a 'Enable API' button. Press that.
    0. In the left pane you will see another link (under APIs & Auth) called Consent Screen. Click on that.
    0. Enter a product name. Let's call it GoogleCalendarSync.
    0. In the left pane you will find the 'Credentials' link (under 'APIs & Auth'), select that.
    0. Now click 'Create new Client ID'.
        0. In the pop-up, select 'Web  Application' radio buttion.
        0. In the 'Authorized JavaScript Origins': put 'http://localhost:8080'
        0. In the 'Authorized Redirect URIs' put 'http://localhost:8080/index.html'
    0. Once the Credentials are generated, copy the 'Cliend ID'.
0. Clone the repository in a local directory using command : 'git clone https://siddharthgupta1986@bitbucket.org/siddharthgupta1986/googlecalendarsync.git'.
0. Traverse to the index.html and right at the top there would be a field called 'CLIENT_ID', paste your Client ID into the field.
0. Start and HTTP Server with the directory containing the cloned repo as the base directory. To do this you can also choose to use the Python's SimpleHTTPServer as follows:
    0. Make sure you have python installed.
    0. Traverse to the directory containing the cloned git repo.
    0. Run command: 'python -m SimpleHTTPServer 8080'.
0. Go to a browser window and in the address bar put : 'http://localhost:8080'.
0. You will be asked to authorize your google accout. Once you have done that, you will see the JQuery's FullCalendar and a list of your google calendars (for your account) below that.
0. Check/Uncheck calendars to get the events rendered on the FullCalendar.
